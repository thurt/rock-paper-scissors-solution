let wins = 0;
let losses = 0;
let ties = 0;

// this function
// 1. returns "rock"/"paper"/"scissors" based on a random integer
function computersMove() {
  // creates a random integer between 0 and 2
  const randInt = Math.floor(Math.random() * 3);
  switch (randInt) {
    case 0:
      return "rock";
    case 1:
      return "paper";
    case 2:
      return "scissors";
  }
}

// this function
// 1. increments the wins/ties/losses depending on the result
// 2. updates the number of wins/ties/losses displayed on the website
function updateScore(result) {
  switch (result) {
    case "won":
      wins++;
      document.getElementById("wins").innerHTML = wins;
      break;
    case "tied":
      ties++;
      document.getElementById("ties").innerHTML = ties;
      break;
    case "lost":
      losses++;
      document.getElementById("losses").innerHTML = losses;
      break;
  }
}

// this function is run when the user clicks on the rock
function onClickRock() {
  switch (computersMove()) {
    case "rock":
      updateScore("tied");
      break;
    case "paper":
      updateScore("lost");
      break;
    case "scissors":
      updateScore("won");
      break;
  }
}

function onClickPaper() {
  switch (computersMove()) {
    case "rock":
      updateScore("won");
      break;
    case "paper":
      updateScore("tied");
      break;
    case "scissors":
      updateScore("lost");
      break;
  }
}

function onClickScissors() {
  switch (computersMove()) {
    case "rock":
      updateScore("lost");
      break;
    case "paper":
      updateScore("won");
      break;
    case "scissors":
      updateScore("tied");
      break;
  }
}

document.getElementById("rock").onclick = onClickRock;
document.getElementById("paper").onclick = onClickPaper;
document.getElementById("scissors").onclick = onClickScissors;
